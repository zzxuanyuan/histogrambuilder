histogrambuilder - Build Histograms from JSON files
===================================================

Install
-------

From pip, run::

    $ pip install histogrambuilder

From the repository, run::

  python setup.py install

to install histogrambuilder on your system.

histogrambuilder is compatible with Python 2.7.


Run Tests
---------

To run unit tests run::

  pytest -s tests/test_* (choose which python file you want to test)


Progress Status
---------------

[Y] 1. Parser module takes JSON file as input and create two internal structures: step_stats and execution_stats. step_stats is a dictionary that maintains execution times for each step. execution_stats is a dictionary that store executions where key is execution id for a execution and value is that execution.

[Y] 2. Implemented basic command line tool. The command line should be: histogrambuilder [FILENAME].

[Y] 3. In case that execution variances are too large, histogrammer plots two figures: one is in normal scale and the other is in log scale.

[N] 4. Command line tool add more arguments for future extending the functionalities. Command options includes: --max, --error, --average. max is a boolean value that indicates builder to plot max execution time across all executions for each step. average indicates to plot average execution. error tell the builder to plot error bars on top of histogram.

[N] 5. For future work, we could add more statistical plotting functions in histogrammer.py and export functions to parser.py like I did as plot_execution_mean(). Since step_stats, execution_stats in parser module are already includes enough presentable data (to the best of my knowledge). Further plotting functions should be not difficult to be built on top of those two data structures.
