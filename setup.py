#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSD-3-Clause

import re

from setuptools import setup, find_packages

def get_version():
	"""Parse __init__.py for version number instead of importing the file."""
	VERSIONFILE = 'histogrambuilder/__init__.py'
	VSRE = r'^__version__ = [\'"]([^\'"]*)[\'"]'
	with open(VERSIONFILE) as f:
		verstrline = f.read()
	mo = re.search(VSRE, verstrline, re.M)
	if mo:
		return mo.group(1)
	raise RuntimeError('Unable to find version in {fn}'.format(fn=VERSIONFILE))


LONG_DESCRIPTION = """
``histogrambuilder`` is a module that takes JSON file and plot a histogram of all executions in it.
"""

setup(
	name='histogrambuilder',
	version=get_version(),
	author='Zhe Zhang',
	author_email='zzxuanyuan@gmail.com',
	description='Histogram Builder',
	long_description=LONG_DESCRIPTION,
	license='BSD',
	classifiers=[
		'Development Status :: 0.1',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: BSD License',
		'Operating System :: OS Independent',
		'Programming Language :: Python',
		'Programming Language :: Python :: 2',
		'Programming Language :: Python :: 2.7',
		'Topic :: LogicHub',
		'Topic :: Interview',
		'Topic :: Software Development',
		],
	packages=find_packages(exclude=('tests',)),
	install_requires=[
		'numpy',
		'matplotlib',
		'setuptools',
		'pytest',
	],
	entry_points={
		'console_scripts': [
			'histogrambuilder = histogrambuilder.__main__:main',
		]
	},
)
