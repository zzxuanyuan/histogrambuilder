import pytest

import os

import histogrambuilder
import histogrambuilder.parser
import histogrambuilder.util

def test_very_large_variances():
	dir_path = os.path.dirname(os.path.realpath(__file__))
	file_path = dir_path + "/data/very_large_variances.json"
	parser = histogrambuilder.parser.Parser()
	parser.read_file(file_path)
	parser.plot_execution_mean()

def test_large_dataset():
	dir_path = os.path.dirname(os.path.realpath(__file__))
	file_path = dir_path + "/data/large_data_set.json"
	parser = histogrambuilder.parser.Parser()
	parser.read_file(file_path)
	parser.plot_execution_mean()
