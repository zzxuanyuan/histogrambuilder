import pytest

import histogrambuilder
import histogrambuilder.util

def test_rsort_dict_by_value_lists():
	test_dict = {'step1':1,'step2':2,'step3':3,'step4':4}
	expected_keys = ('step4','step3','step2','step1')
	expected_values = (4,3,2,1)
	keys,values = histogrambuilder.util.rsort_dict_by_value_lists(test_dict)
	assert(keys == expected_keys)
	assert(values == expected_values)

