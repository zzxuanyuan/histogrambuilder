#!/usr/bin/env python
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSD-3-Clause

import argparse
import sys

import histogrambuilder

def create_histogram_builder():
	parser = argparse.ArgumentParser(
		prog='histogrambuilder',
		description='Build Histogram from JSON that contains executions',
		usage='%(prog)s [OPTIONS] FILE, ...',
	)

	parser.add_argument('filename', help='JSON file that contains execution steps')
	parser.add_argument('-x', '--max', action='store_false',help='plot histogram of max execution time')
	parser.add_argument('-a', '--average', action='store_false',help='plot histogram of average execution time')
	parser.add_argument('-e', '--error', action='store_false',help='plot error bars for a given step between executions')
	parser.add_argument('-v', '--version', action='version',version=histogrambuilder.__version__)

	return parser

def main(args=None):
	builder = create_histogram_builder()
	args = builder.parse_args(args)
	
	opts = vars(args)
	histogrambuilder.format(**opts)	
	return 0
