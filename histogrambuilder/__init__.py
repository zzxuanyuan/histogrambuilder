#!/usr/bin/env python
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSD-3-Clause

import histogrambuilder.parser

__version__ = '0.1.0'

def format(**opts):
	parser = histogrambuilder.parser.Parser()
	parser.read_file(opts['filename'])
	parser.plot_execution_mean()
	return 0
