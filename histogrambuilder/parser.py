#!/usr/bin/env python
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSD-3-Clause

import json_formatter
import histogrammer
import numpy as np

class Execution:
	def __init__(self, step_dict):
		self.step_dict = step_dict

	def dump(self):
		print self.step_dict

class Parser:
	def __init__(self):
		self.histogrammer = histogrammer.Histogrammer()
		self.execution_stats = dict()
		self._tmp_step_stats = dict()
		self.step_stats = dict()

	def _parse_to_stats(self, idx, steps):
		step_dict = dict()
		for step in steps:
			step_dict[step['name']] = step['timeMs']
			if step['name'] in self._tmp_step_stats:
				self._tmp_step_stats[step['name']].append(step['timeMs'])
			else:
				self._tmp_step_stats[step['name']] = [step['timeMs']]
		execution = Execution(step_dict)
		self.execution_stats[idx] = execution

	def read_file(self, filename):
		json_file = open(filename, 'r')
		data = json_formatter.json_load_byteified(json_file)
		for i in data['executions']:
			execution = self._parse_to_stats(i['id'],i['steps'])
		for l in self._tmp_step_stats:
			self.step_stats[l] = np.array(self._tmp_step_stats[l])
		self._tmp_step_stats.clear()

	def get_step_stats(self):
		return self.step_stats

	def get_execution_stats(self):
		return self.execution_stats

	def dump(self):
		print self.step_stats
		print self.execution_stats

	def plot_execution_mean(self):
		self.histogrammer.plot_execution_mean(self.step_stats)
