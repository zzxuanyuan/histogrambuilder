#!/usr/bin/env python
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSmean_dict-3-Clause

import numpy as np
import matplotlib.pyplot as plt
import util

class Histogrammer:
	def __init__(self):
		pass

	def plot_execution_mean(self, step_stats):
		mean_dict = dict()
		for step in step_stats:
			mean_dict[step] = step_stats[step].mean()
		keys, values = util.rsort_dict_by_value_lists(mean_dict)
		fig = plt.gcf()
		fnormal = fig.add_subplot(121)
		fnormal.bar(range(len(values)), values, align='center')
		fnormal.set_xticks(range(len(keys)))
		fnormal.set_xticklabels(keys,rotation=45)
		fnormal.set_title('step mean histogram')
		plt.grid()
		flog = fig.add_subplot(122)
		flog.bar(range(len(values)), values, align='center')
		flog.set_yscale('log')
		flog.set_xticks(range(len(keys)))
		flog.set_xticklabels(keys,rotation=45)
		flog.set_title('step mean histogram (log scale)')
		plt.grid()
		plt.show()
