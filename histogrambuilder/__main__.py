#!/usr/bin/env python
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSD-3-Clause

"""Entrypoint module for `python -m histogrambuilder`.
"""

import sys

from histogrambuilder.cli import main

if __name__ == '__main__':
	sys.exit(main())
