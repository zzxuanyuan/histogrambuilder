#!/usr/bin/env python
#
# Copyright (C) 2017 Zhe Zhang, zzxuanyuan@gmail.com
#
# the BSD License: https://opensource.org/licenses/BSmean_dict-3-Clause

import operator

def rsort_dict_by_value_lists(x):
	rsorted_tuple = sorted(x.items(), key=operator.itemgetter(1), reverse=True)
	key, value = zip(*rsorted_tuple)
	return key, value
